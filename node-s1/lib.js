// var module = (function () {

  // Esta variable es privada
//   var privateProperty = 'Soy una variable privada!!';

//   return {
//     publicProperty: 'Soy una variable publica', publicMethod: function (args) {
      // do something
//       console.log("Metodo publico " + provateProperty);
//     },
//   };
// })();

// console.log(moduole.privateProperty); // undefined
// console.log(module.publicProperty); // Soy una variable publica
// module.publicMethod(); // Metodo publico Soy una variable privada!!!


//? Module loader          de Node.js
// import
// require() -> commonjs


// module.exports.miFuncion = () => {}
// module.exports = () => {}
// exports.miFuncion = () => {}

let miVarLocal = 5;

const miFunLocal = () => {
  console.log(`Soy miFunLocal`);
};

module.exports = {
  miVarLocal,
  miFunLocal
}
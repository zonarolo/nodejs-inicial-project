const http = require('http');
const url = require('url');


const requestHandler = (request, response) => {
  // console.log(request.url);
  // console.log(request.method);

  const fs = require('fs');
  const fileName = "movies.json"

  const updateMovies= (movies) => {
    const callback = () => {
      console.log("Fichero actualizado");
    }
    fs.writeFile(fileName, JSON.stringify(movies), callback);
  }
  
  
  const readMovies = (callbackReadMovies) => {
    console.log('readMovies ha sido invocada');
    const callbackReadFile = (err, data) => {
      console.log("readFile ha terminado de leer el fichero");
      err ? console.log(err) : callbackReadMovies(JSON.parse(data,toString()));
    }
  
    fs.readFile(fileName, callbackReadFile);
  
  };
  
  
  readMovies( (json) => {

    let status = 200;
    let result;


    if (request.method == "GET" && request.url == "/movies"){

      response.writeHead(status);
      response.end(JSON.stringify(json));

    } else if (request.method == "GET" && request.url == "/movies/1"){
      response.writeHead(status);

      //TODO: devolver una sola,
 
      response.end(JSON.stringify(json[1]));
    

    } else {
      status = 404;
      response.end(status)
    }

    response.writeHead(status);
    response.end(result);
  })

}

const server = http.createServer(requestHandler);

server.listen(3000, () => {
  console.log('Server runnning!');
});

//!--------------------------
// const events = require('events);
// server.on('request', manejador);
// server.on('error', manejador);
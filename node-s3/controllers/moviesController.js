const dbb = require("../lib/dbb");


//!----------------------------------------------------------------------------------
const getMovie = (req, res) => {
  console.log("Estoy dentro del GET");
  const db = client.db("test");
  const collection = db.collection("movies");


  if (req.params.id) {
    collection.findOne({name: "Ant man"}, function (err, result) {
      if (err) throw err;
      res.json(result);
      client.close();
    });
  } else {
    collection.find().toArray(function (err, result) {
      if (err) throw err;
      res.json(result);
      client.close();
    });
  }
    
};
//!-----------------------------------------------------------------------------------

const updateMovie = (req, res) => {
  let nombre = req.body.nombre;
  let año = req.body.año;

  console.log(nombre, año);
  dbb.readMovies((json) => {
    json.push({name: nombre, year: año});

    dbb.updateMovies(json);
  });

  res.send(
    "<h1>JSON actializado correctamente</h1><a href='/movies'>Mirar el JSON</a>"
  );
};

//!----------------------------------------------------------------------------------------
const postMovie = (req, res) => {
  console.log("Estoy dentro del POST");
  // console.log(req.body, name);
  // res.json({nombre: "Iron man"});

  //TODO: Agregar a BBDD
  const db = client.db("test");
  const collection = db.collection("movies");
  console.log(req.body);
  collection.insertOne(req.body, {}, (err) => {
    if (err) throw err;
    console.log("1 document inserted");
    client.close();
  });
};

//!---------------------------------------------------------------------------------------
const putMovie = (req, res) => {
  console.log("Estoy dentro del PUT");
  //TODO: Modificar un elemento de la BBDD
   
    const db = client.db("test");
    const collection = db.collection("movies");
    var myquery = {"_id": "5ec7abbc80a67e85af62474c"};
    var newvalues = { $set: {name: "star wars", year: "1976" } };
    collection.updateOne(myquery, newvalues, function(err, res) {
      if (err) throw err;
      console.log("1 document updated");
      client.close();
    });
  
};

//!---------------------------------------------------------------------------------------
const deleteMovie = (req, res) => {
  console.log("Estoy dentro del DELETE");
  //TODO: Eliminar de BBDD
  const db = client.db("test");
  const collection = db.collection("movies");

  if(req.params.id){
    collection.find().toArray(function (err, result) {
      if (err) throw err;
      let query = {_id: result[req.params.id-1]._id};
      collection.deleteOne(query);
      client.close();
    });
  }

};

module.exports = {
  getMovie,
  updateMovie,
  postMovie,
  putMovie,
  deleteMovie,
  dbConnect,
};

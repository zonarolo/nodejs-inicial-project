const express = require('express');
const bodyParser = require("body-parser");

const moviesRoutes = require('./routes/movies');

const app= express();
const port = 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));


app.use('/movies', moviesRoutes);


app.listen(port, () => console.log(`Server running on port ${port}`));
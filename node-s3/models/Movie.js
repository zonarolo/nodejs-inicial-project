const mongoose = require('mongoose');

const MovieSchema = new mongoose.Schema(
  {
    id: {type: Number, required: true },
    name: {type: String, required: true},
    year: {type: String, required: true}
  },
  {
    timestamps: true
  }
);

const Movie = mongoose.model('Movie', MovieSchema);

module.exports = Movie;
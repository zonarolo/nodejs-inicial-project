const fs = require('fs');
const fileName = "movies.json"

const updateMovies= (movies) => {
  const callback = () => {
    console.log("Fichero actualizado");
  }
  fs.writeFile(fileName, JSON.stringify(movies), callback);
}


const readMovies = (callbackReadMovies) => {
  console.log('readMovies ha sido invocada');
  const callbackReadFile = (err, data) => {
    console.log("readFile ha terminado de leer el fichero");
    err ? console.log(err) : callbackReadMovies(JSON.parse(data,toString()));
  }

  fs.readFile(fileName, callbackReadFile);

};

module.exports = {
  updateMovies,
  readMovies
};
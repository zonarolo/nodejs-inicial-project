const mongoose = require('mongoose');
const dbConnectionURI = require("../config.json").uriMongoose;

module.exports = (req, res, next) => {
  mongoose.connect(dbConnectionURI, {useNewUrlParser: true,  useUnifiedTopology: true },  function (err, clientLocal) {
    if (err) throw err;
    console.log("Connected successfully to server");
    client = clientLocal;
    next();
  });
};